<?php

use yii\db\Migration;

/**
 * Class m190113_080130_win
 */
class m190113_080130_win extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('win', [
            'id' => $this->primaryKey(),
			'id_user' => $this->string(),
            'type' => $this->string(),
            'value' => $this->string(200),
            
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190113_080130_win cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190113_080130_win cannot be reverted.\n";

        return false;
    }
    */
}
