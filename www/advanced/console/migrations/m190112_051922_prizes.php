<?php

use yii\db\Migration;

/**
 * Class m190112_051922_prizes
 */
class m190112_051922_prizes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('prizes', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'value' => $this->string(200),
            
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190112_051922_prizes cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190112_051922_prizes cannot be reverted.\n";

        return false;
    }
    */
}
