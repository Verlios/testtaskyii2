<?php 
namespace frontend\controllers;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\Prizes;
use frontend\models\User;
use frontend\models\Win;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\httpclient\Client;

class PrizesController extends Controller
{
	public function actionIndex()
	{
		$id_user=Yii::$app->user->id;
		if (Yii::$app->user->isGuest) {//проверяем авторизирован ли пользователь
            return $this->goHome();
        }
		else{
		$id_p=rand(1,Prizes::find()->count());//выбираем рандомный приз
		$prizes = Prizes::find()->all();
		 foreach($prizes as $prize){
			 if($prize->id==$id_p){
			 $type=$prize->type;
			 $value=$prize->value;
			   $win_model= new Win;//вносим данные в общую таблицу
			   $win_model->id_user=$id_user;
			   $win_model->type= $type;
			   $win_model->value=$value;
			   $win_model->save();
			 }
		 }
		
		 if($type=='points'){// если бонусы то начисляем их на счет пользователя
			 $model_user = User::find()->where(['id' => $id_user])->one();
			 $model_user->bonus+=$value;
			  $model_user->save();
		 }
		

			return $this ->render('prizes',[// запускаем отображение и передаем данные о призе
		   'type' =>$type,
'value'=>$value
	
		]);
		}
	}
	
	public function actionPrize()
	{
		return $this ->render('prize',[
		  

	
		]);
	}
	
}